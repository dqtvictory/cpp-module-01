#include <iostream>
#include <fstream>

static void	editFile(std::ifstream &infile, std::ofstream &outfile,
					std::string to_find, std::string to_replace)
{
	std::string	line;
	size_t		pos, len;

	len = to_find.length();
	while (std::getline(infile, line))
	{
		while (true)
		{
			pos = line.find(to_find);
			if (pos != std::string::npos)
				line.replace(pos, len, to_replace);
			else
				break;
		}
		outfile << line;
		if (!infile.eof())
			outfile << '\n';
	}

}

static int	printError(void)
{
	std::cout	<< "Usage: ./replace fname s1 s2, in which: \n"
				<< "- fname: name of the file to be editted,\n"
				<< "- s1: the non-empty string to be replaced in fname, and\n"
				<< "- s2: the non-empty string that replaces s1 in fname.\n";
	return (1);
}

int	main(int ac, char **av)
{
	if (ac != 4)
		return (printError());
	
	std::ifstream	infile(av[1]);
	if (!infile)					// Test if file is opened
	{
		std::cerr	<< av[1] << ": Cannot open file\n";
		return (1);
	}
	else if (*av[2] == 0 || *av[3] == 0)	// Test if strings are not empty
	{
		infile.close();
		return (printError());
	}
	
	std::ofstream	outfile((std::string(av[1]) + std::string(".replace")).c_str());
	if (!outfile)
	{
		infile.close();
		std::cerr	<< av[1] << ".replace: Cannot create new file\n";
		return (1);
	}

	editFile(infile, outfile, av[2], av[3]);

	infile.close();
	outfile.close();
}
