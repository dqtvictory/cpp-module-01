#include <iostream>
#include "Karen.hpp"

const char	*COLOR[] = {
	"\033[0m",		// 0 NO COLOR
	"\033[95m",		// 1 PURPLE
	"\033[94m",		// 2 BLUE
	"\033[96m",		// 3 CYAN
	"\033[92m",		// 4 GREEN
	"\033[93m",		// 5 YELLOW
	"\033[91m",		// 6 RED
};

const std::string
DEBUG_MSG = "[ DEBUG ]\nI love to get extra bacon \
for my 7XL-double-cheese-triple-pickle-\
special-ketchup burger. I just love it!\n";

const std::string
INFO_MSG = "[ INFO ]\nI cannot believe adding extra \
bacon cost more money. You don’t put enough! \
If you did I would not have to ask for it!\n";

const std::string
WARN_MSG = "[ WARNING ]\nI think I deserve to have \
some extra bacon for free. I’ve been coming here for \
years and you just started working here last month.\n";

const std::string
ERROR_MSG = "[ ERROR ]\nThis is unacceptable, I want to \
speak to the manager now.\n";

const std::string
INSIGNIFICANT = "[ Probably complaining about \
insignificant problems ]\n";

void	Karen::debug(void)
{
	std::cout	<< COLOR[1] << DEBUG_MSG << COLOR[0];
}

void	Karen::info(void)
{
	std::cout	<< COLOR[4] << INFO_MSG << COLOR[0];
}

void	Karen::warning(void)
{
	std::cout	<< COLOR[5] << WARN_MSG << COLOR[0];
}

void	Karen::error(void)
{
	std::cout	<< COLOR[6] << ERROR_MSG << COLOR[0];
}

void	Karen::insignificant(void)
{
	std::cout	<< INSIGNIFICANT;
}

Karen::Karen()
{
	arr[0] = &Karen::debug;
	arr[1] = &Karen::info;
	arr[2] = &Karen::warning;
	arr[3] = &Karen::error;
	arr[4] = &Karen::insignificant;
}

Karen::~Karen()
{}

void	Karen::complain(std::string level)
{
	const std::string levels[] = {"DEBUG", "INFO", "WARNING", "ERROR"};
	
	int	i = 0;
	while (i < 4 && level != levels[i])
		i++;
	(*this.*arr[i])();
}
