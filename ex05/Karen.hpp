#ifndef KAREN_HPP
#define KAREN_HPP

#include <string>

class Karen
{

typedef void	(Karen::*func)(void);

private:
	void	debug(void);
	void	info(void);
	void	warning(void);
	void	error(void);
	void	insignificant(void);

public:
	Karen();
	~Karen();
	void	complain(std::string level);
	func	arr[5];

};

#endif
