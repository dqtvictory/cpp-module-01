#include <iostream>
#include <cstdlib>
#include <ctime>
#include "Zombie.hpp"

#define N_ZOMBIE 4

void	randomChump(std::string name);
Zombie	*newZombie(std::string name);

const std::string
ZOMBIE_NAME[] = {
	"The Bocal",
	"Johnny",
	"Melissa",
	"T-Rex",
	"Jimmy",
	"Andy",
	"Tony"
};

int	main()
{
	const size_t	n_names = sizeof(ZOMBIE_NAME) / sizeof(std::string);
	Zombie	*zom[N_ZOMBIE];
	
	std::srand(time(NULL));

	std::cout << "\n//////// Zombie on the heap ////////\n\n";
	for (int i = 0; i < N_ZOMBIE; i++)
	{
		zom[i] = newZombie(ZOMBIE_NAME[std::rand() % n_names]);
		zom[i]->announce();
	}
	for (int i = 0; i < N_ZOMBIE; i++)
		delete zom[i];

	std::cout << "\n//////// Zombie on the stack ////////\n\n";
	for (int i = 0; i < N_ZOMBIE; i++)
		randomChump(ZOMBIE_NAME[std::rand() % n_names]);
}
