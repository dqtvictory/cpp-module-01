#include <iostream>
#include <cstdlib>
#include "Zombie.hpp"

const char	*COLOR[] = {
	"\033[0m",		// NO COLOR
	"\033[95m",		// PURPLE
	"\033[94m",		// BLUE
	"\033[96m",		// CYAN
	"\033[92m",		// GREEN
	"\033[93m",		// YELLOW
	"\033[91m",		// RED
};

Zombie::Zombie(void) : _name("Unnamed Zombie")
{
	_col = (rand() % 6) + 1;
}

Zombie::Zombie(std::string name) : _name(name)
{
	_col = (rand() % 6) + 1;
}

Zombie::~Zombie(void)
{
	std::cout	<< COLOR[_col]
				<< '<' + _name + '>'
				<< " Arrr... DEAD\n"
				<< COLOR[0];
}

void	Zombie::announce(void)
{
	std::cout	<< COLOR[_col]
				<< '<' + _name + '>'
				<< " BraiiiiiiinnnzzzZ...\n"
				<< COLOR[0];
}

void	Zombie::setName(std::string new_name)
{
	_name = new_name;
}
