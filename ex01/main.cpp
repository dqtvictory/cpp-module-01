#include <cstdlib>
#include <ctime>
#include <iostream>
#include "Zombie.hpp"

#define N_ZOMBIE	5

Zombie	*zombieHorde(int N, std::string name);

const std::string
ZOMBIE_NAME[] = {
	"The Bocal",
	"Johnny",
	"Melissa",
	"T-Rex",
	"Jimmy",
	"Andy",
	"Tony"
};

int main()
{
	const size_t	n_names = sizeof(ZOMBIE_NAME) / sizeof(std::string);

	std::srand(std::time(NULL));
	Zombie	*horde = zombieHorde(N_ZOMBIE, ZOMBIE_NAME[std::rand() % n_names]);
	for (int i = 0; i < N_ZOMBIE; i++)
		horde[i].announce();
	delete[] horde;
}
