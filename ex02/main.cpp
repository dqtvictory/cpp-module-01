#include <iostream>

int main()
{
	std::string	str("HI THIS IS BRAIN");

	std::string	*stringPTR(&str);
	std::string	&stringREF(str);

	std::cout 	<< "Address of string itself: "	<< &str << "\n"
				<< "Address of stringPTR:     "	<< stringPTR << "\n"
				<< "Address of stringREF:     " << &stringREF << "\n\n";

	std::cout	<< "String using ptr: " << *stringPTR << '\n'
				<< "String using ref: " << stringREF << '\n';
}
