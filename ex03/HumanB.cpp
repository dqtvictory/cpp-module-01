#include <iostream>
#include "HumanB.hpp"

HumanB::HumanB(std::string name) :	_name(name),
									_weapon(NULL)
{}

HumanB::~HumanB() {}

void	HumanB::attack(void)
{
	if (_weapon == NULL)
		std::cout << _name << " attacks with his bare hands\n";
	else
		std::cout << _name << " attacks with his " << _weapon->getType() << '\n';
}

void	HumanB::setWeapon(Weapon *weapon)
{
	_weapon = weapon;
}

void	HumanB::setWeapon(Weapon &weapon)
{
	_weapon = &weapon;
}
